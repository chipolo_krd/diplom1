<?php

class DatabaseFactory
{

    private static $factory;
    private $database;

    public static function getFactory()
    {
	if (!self::$factory)
	{
	    self::$factory = new DatabaseFactory();
	}
	return self::$factory;
    }

    public function getConnection()
    {
	$dbhost = "localhost";
	$dbuser = "tsibulnik";
	$dbname = "tsibulnik";
	$dbpass = 'neto0412';
	if (!$this->database)
	{
	    try
	    {

		$this->database = new PDO("mysql:host=$dbhost;dbname=$dbname", "$dbuser", "$dbpass");
	    } catch (PDOException $e)
	    {
		echo 'Database connection can not be estabilished. Please try again later.' . '<br>';
		echo 'Error code: ' . $e->getCode();
		exit;
	    }
	}
	$this->database->exec('SET NAMES utf-8');
	$this->database->exec("SET CHARACTER SET 'utf8';");
	$this->database->exec("SET SESSION collation_connection = 'utf8_general_ci';");
	return $this->database;
    }

}
