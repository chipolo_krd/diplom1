<?php

class Redirect
{

    public static function to($page)
    {
	$host = $_SERVER['HTTP_HOST'];
	$url = "http://" . $host . '/' . $page;
	header("Location: $url");
    }

    public static function back()
    {
	$url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	header("Location: $url");
    }

}
