<?php

class Request
{

    public static function post($key, $clean = FALSE)
    {
	if (isset($_POST[$key]))
	{
	    $clean = stripslashes(strip_tags(trim($_POST[$key])));
	}
	return ($clean);
    }

    public static function get($key, $clean = FALSE)
    {
	if (isset($_GET[$key]))
	{
	    $clean = htmlspecialchars(stripslashes(strip_tags(trim($_GET[$key]))));
	}
	return ($clean);
    }

}
