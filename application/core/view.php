<?php

class View
{

    public static function disp($params, $page)
    {
	$view = 'application/views/';
	Twig_Autoloader::register();
	try
	{
	    $loader = new Twig_Loader_Filesystem($view);
	    $twig = new Twig_Environment($loader);
	    $template = $twig->loadTemplate($page);
	    $host = $_SERVER['HTTP_HOST'];
	    $params['url'] = "http://" . $host;
	    echo $template->render($params);
	} catch (Exception $e)
	{
	    die('ERROR: ' . $e->getMessage());
	}
    }

    public static function params($action, $titlepage, $page, $H1)
    {
	$params['action'] = $action;
	$params['titletable'] = View::getTitleAdminTable($page);
	$params['titlepage'] = $titlepage;
	$params['page'] = $page;
	$params[$action] = $page;
	$params['H1'] = $H1;
	$params['admin'] = Session::get('islogin');
	$params['countnew'] = Question::countQuestion();
	$params['undo'] = $_SERVER['HTTP_REFERER'];
	return($params);
    }

    public static function getTitleAdminTable($page)
    {
	if ($page == 'Admins')
	{
	    $titletable = ['Логин', 'E-mail', 'Дата', 'Действие'];
	}
	if ($page == 'Authors')
	{
	    $titletable = ['Имя', 'E-mail', 'Дата', 'Действие'];
	}
	if ($page == 'Categories')
	{
	    $titletable = ['Наименование категории', 'Опубликованных вопросов',
		'Вопросы без ответа', 'Скрытые вопросы',
		'Всего вопросов', 'Автор', 'Действие'];
	}
	if ($page == 'Questions')
	{
	    $titletable = ['Вопрос', 'Категория', 'Кто создал', 'Статус',
		'Дата', 'Действие'];
	}
	return($titletable);
    }

    public static function countQuestion()
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT count(*) FROM `question` where status='moderate'";
	$query = $database->query($sql);
	$count = $query->fetchAll(PDO::FETCH_ASSOC);
	return($count[0]['count(*)']);
    }

}
