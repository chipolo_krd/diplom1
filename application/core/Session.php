<?php

class Session
{

    public static function init()
    {

	if (session_id() == '')
	{
	    session_start();
	}
    }

    public static function set($key, $value)
    {
	$_SESSION[$key] = $value;
    }

    public static function get($key)
    {
	if (isset($_SESSION[$key]))
	{
	    $value = $_SESSION[$key];
	    return ($value);
	}
	return false;
    }

    public static function del($key)
    {
	if (isset($_SESSION[$key]))
	{
	    unset($_SESSION[$key]);
	    return (true);
	}
	return false;
    }

    public static function add($key, $value)
    {
	$_SESSION[$key][] = $value;
    }

    public static function destroy()
    {
	session_destroy();
    }

    public static function checkauth()
    {
	if (isset($_SESSION['islogin']))
	{
	    return(Session::get('login'));
	}
	Redirect::to('admin/login');
    }

}
