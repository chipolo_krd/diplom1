<?php

class Route
{

    static function start()
    {
	$controller_name = 'Index';
	$action = 'index';
	$routes = explode('/', $_SERVER['REQUEST_URI']);
	if (!empty($routes[1]))
	{
	    $controller_name = $routes[1];
	}
	if (!empty($routes[2]))
	{
	    $action = $routes[2];
	}
	$model_name = $controller_name . 'Model';
	$controller_name = $controller_name . 'Controller';
	$model_file = strtolower($model_name) . '.php';
	$model_path = getcwd() . "/application/models/" . $model_file;
	if (file_exists($model_path))
	{
	    include "$model_path";
	}
	$controller_file = strtolower($controller_name) . '.php';
	$controller_path = getcwd() . "/application/controllers/" . $controller_file;
	if (file_exists($controller_path))
	{

	    include "$controller_path";
	}
	else
	{
	    Route::ErrorPage404();
	}
	$controller = new $controller_name;
	if (method_exists($controller, $action))
	{
	    $controller->$action();
	}
	else
	{
	    Route::ErrorPage404();
	}
    }

    function ErrorPage404()
    {
	$host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
	header('HTTP/1.1 404 Not Found');
	header("Status: 404 Not Found");
	header('Location:' . $host . '404');
    }

}
