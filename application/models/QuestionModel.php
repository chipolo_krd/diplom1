<?php

class Question
{

    public static function get($category_id = false)
    {
	$database = DatabaseFactory::getFactory()->getConnection();

	$sql = "SELECT REPLACE(question.question, 'n', '<br/>') as question, 
		question.id_question, question.status,  question.date, 
		category.category, category.id_category, users.name, users.email
                FROM category 
                INNER JOIN question ON category.id_category = question.category_id
		INNER JOIN users ON question.author_id = users.id_users";
	if ($category_id !== False)
	{
	    $sql.=" where question.id_question='$category_id'";
	}
	$query = $database->prepare($sql);
	$query->execute(array(':category_id' => $category_id));
	if ($query->rowCount() >= 1)
	{
	    return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	return false;
    }

    public static function getUnreadQuestion()
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT question.question, question.id_question, question.status, 
	    question.date, category.category, users.name
                FROM category 
                INNER JOIN question ON category.id_category = question.category_id
		INNER JOIN users ON question.author_id = users.id_users
		where question.status='moderate'
		ORDER BY `question`.`date` DESC";
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function set($question, $id_category, $id_author, $publish = "moderate")
    {
	if (empty($question) || empty($id_category) || empty($id_author) || !is_numeric($id_category))
	{
	    return false;
	}
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "INSERT INTO question (question, category_id, author_id, status,  date) 
                VALUES ('$question', '$id_category', '$id_author', '$publish',  now())";
	$query = $database->exec($sql);
	return($query);
    }

    public static function update($question_id = false, $question = false, $category_id = FALSE, $status = FALSE)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "UPDATE question 
	    SET question = '$question',
	    category_id = '$category_id',
	    status = '$status'
	    WHERE id_question = $question_id";
	$query = $database->query($sql);
	return ($query);
    }

    public static function deleteForCategory($category_id = false)
    {
	if (!$category_id || !is_numeric($category_id))
	{
	    return false;
	}
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "DELETE FROM question
                WHERE category_id = :category_id ";
	$query = $database->prepare($sql);
	$query->execute(array(':category_id' => $category_id));
	if ($query->rowCount() >= 1)
	{
	    return true;
	}
	return false;
    }

    public static function delete($id = false)
    {
	if (!$id || !is_numeric($id))
	{
	    return false;
	}
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "DELETE FROM question
                WHERE id_question = :id LIMIT 1";
	$query = $database->prepare($sql);
	$query->execute(array(':id' => $id));
	if ($query->rowCount() == 1)
	{
	    return true;
	}
	return false;
    }

    public static function countQuestion($status = FALSE, $id_category = FALSE)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	if ($status == FALSE && $id_category == FALSE)
	{
	    $sql = "SELECT count(*) AS count FROM `question` where status='moderate'";
	    $query = $database->query($sql);
	    $count = $query->fetchAll(PDO::FETCH_ASSOC);
	    return($count[0]['count']);
	}
	elseif ($status !== FALSE && $id_category !== FALSE)
	{
	    $sql = "SELECT COUNT( * ) AS count
		FROM  `question` 
		WHERE  `category_id` =  '$id_category'
		AND  `status` =  '$status'";
	    $query = $database->query($sql);
	    $count = $query->fetchAll(PDO::FETCH_ASSOC);
	    return($count[0]['count']);
	}
    }

}
