<?php

class FAQ
{

    public static function getIndexData()
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = 'SELECT '
		. '`d`.`id_category`,`d`.category, `b`.`answer`, `a`.`question`, `c`.`name`, `b`.`date`, `a`.`status` '
		. 'FROM `answer` as `b` '
		. 'JOIN `users` as `c` ON `c`.id_users = `b`.`author_id` '
		. 'JOIN `question` as `a` ON `a`.id_question = `b`.question_id '
		. 'JOIN `category` as `d` ON `d`.id_category= `a`.`category_id` '
		. 'ORDER BY `category` LIMIT 50;';
	foreach ($database->query($sql)as $row)
	{
	    $params[$row['id_category']][] = [
		category => $row['category'],
		answer => $row['answer'],
		question => $row['question'],
		date => $row['date'],
	    ];
	}
	return $params;
    }

    public static function getFAQ()
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = 'SELECT '
		. '`d`.`id_category`,`d`.category, `b`.`answer`, `a`.`question`, `c`.`name`, `b`.`date`, `a`.`status` '
		. 'FROM `answer` as `b` '
		. 'JOIN `users` as `c` ON `c`.id_users = `b`.`author_id` '
		. 'JOIN `question` as `a` ON `a`.id_question = `b`.question_id '
		. 'JOIN `category` as `d` ON `d`.id_category= `a`.`category_id` '
		. 'ORDER BY `category` LIMIT 50;';
	foreach ($database->query($sql)as $row)
	{
	    if ($row['status'] == "posted")
	    {
		$params[$row['id_category']]['category_name'] = $row['category'];
		$params[$row['id_category']]['questions'][] = [
		    'answer' => $row['answer'],
		    'question' => $row['question'],
		    'date' => $row['date'],
		];
	    }
	}
	return($params);
    }

    public static function getCategory()
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT id_category, category 
                 FROM category";
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

}
