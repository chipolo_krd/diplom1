<?php

class Category
{

    public static function get($id = false)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT `c`.`id_category`, `c`.`category`, `a`.`login` 
		FROM `category` as `c` 
		JOIN `admins` as `a` on `c`.`author_id`= `a`.`id_admin`";
	if (is_numeric($id))
	{
	    $sql = "SELECT `c`.`id_category`, `c`.`category`, `a`.`login`
	    FROM `category` as `c`
	    JOIN `admins` as `a` on `c`.`author_id` = `a`.`id_admin`
	    where `c`.`id_category` = $id";
	}
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function set($category = false, $author_id = 1)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "INSERT INTO category (category, author_id) 
                VALUES ('$category', '$author_id')";
	$database->query($sql);
    }

    public static function update($category = false, $category_id = false)
    {
	if (!$category || !$category_id)
	{
	    return false;
	}

	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "UPDATE category
                SET category = :category
                WHERE id_category = :category_id LIMIT 1";
	$query = $database->prepare($sql);
	$query->execute(array(':category' => $category,
	    ':category_id' => $category_id,
	));
	if ($query->rowCount() == 1)
	{
	    return true;
	}
	return false;
    }

    public static function delete($category_id = false)
    {
	if (!$category_id)
	{
	    return false;
	}
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "DELETE FROM category 
                WHERE id_category = :category_id LIMIT 1";
	$query = $database->prepare($sql);
	$query->execute(array(':category_id' => $category_id));
	if ($query->rowCount() == 1)
	{
	    Question::deleteForCategory($category_id);
	    return true;
	}
	return false;
    }

}
