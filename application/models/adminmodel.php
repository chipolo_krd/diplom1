<?php

class Admin
{

    public static function get($id)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT login, password, id_admin, email, date
                FROM admins";
	if (is_numeric($id))
	{
	    $sql.=" where id_admin='$id'";
	}
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function auth($admin_pass, $admin_login)
    {
	$database = DatabaseFactory::getFactory()->getConnection();

	$sql = "SELECT `login`, `password`
		FROM `admins`
                WHERE `login` = '$admin_login' LIMIT 1";
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function set($admin_login, $admin_password, $email, $author_id = 1)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "INSERT INTO admins( login,
		password , email, author_id, DATE )
		VALUES (
		'$admin_login', '$admin_password', '$email', '$author_id', NOW( ))";
	$query = $database->exec($sql);
	return($query);
    }

    public static function delete($id_admin = false)
    {
	if (!$id_admin && !is_numeric($id_admin))
	{
	    return false;
	}
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "DELETE FROM admins 
                WHERE id_admin = :id_admin LIMIT 1";
	$query = $database->prepare($sql);
	$query->execute(array(':id_admin' => $id_admin));
	if ($query->rowCount() == 1)
	{
	    return true;
	}
	return false;
    }

    public static function update($password_admin, $login_admin, $id_admin, $admin_email)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "UPDATE admins
                SET login = '$login_admin',
		password = '$password_admin',
		email= '$admin_email'
                 WHERE id_admin = '$id_admin' LIMIT 1";
	$query = $database->exec($sql);
	return($query);
    }

}
