<?php

class Answer
{

    public static function get($question_id = false)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "SELECT q.question, a.answer, a.id_answer, a.date
                FROM answer as `a`
                 JOIN question as `q` ON `a`.`question_id` = `q`.`id_question`
                WHERE `a`.`question_id` = $question_id";
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function update($answer = false, $question_id = FALSE, $answer_id = FALSE)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "UPDATE `answer` "
		. "SET `answer` = '$answer',"
		. " `question_id` = '$question_id'"
		. " WHERE `answer`.`id_answer` = $answer_id;";
	$query = $database->query($sql);
	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function set($answer = false, $id_question = false, $author_id = 1)
    {
	$database = DatabaseFactory::getFactory()->getConnection();
	$sql = "INSERT INTO `answer` (`answer`, `question_id`, `author_id`, `date`) VALUES ('$answer', '$id_question', '$author_id', now() )";
	$query = $database->query($sql);
	return ($query);
    }

}
