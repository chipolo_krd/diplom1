<?php

class IndexController extends Controller
{

    public function __construct()
    {
	parent::__construct();
    }

    public function index()
    {
	$params['titlepage'] = "Сервис вопросов и ответов";
	$params['H1'] = "FAQ";
	$params['faq'] = FAQ::getFAQ();
	$params['action'] = "view";
	View::disp($params, "index.twig");
    }

    public function newQuestion()
    {
	$params['titlepage'] = "Задать вопрос";
	$params['H1'] = "Спроси меня";
	$params['categories'] = FAQ::getCategory();
	$params['action'] = "new";
	View::disp($params, "index.twig");
    }

    public function addQuestion()
    {
	$question = Request::post('question');
	$id_category = Request::post('category_id');
	$name = Request::post('user_name');
	$email = Request::post('email');
	$id_authors = Authors::set($email, $name);
	if (is_numeric($id_category) && !empty($id_authors) && !empty($question))
	{
	    Question::set($question, $id_category, $id_authors);
	}
	Redirect::to('');
    }

}
