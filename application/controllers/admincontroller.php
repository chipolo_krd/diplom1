<?php

class AdminController extends Controller
{

    public function __construct()
    {
	parent::__construct();
    }

    public function login()
    {
	$admin_name = Request::post('admin_name');
	$admin_pass = Request::post('admin_pass');
	if ($admin_name !== FALSE && $admin_pass !== FALSE)
	{
	    $admin_pass = crypt($admin_pass, qwer);
	    $account = Admin::auth($admin_pass, $admin_name);
	    $login = $account[0]['login'];
	    $password = $account[0]['password'];
	    if ($login == $admin_name && $password == $admin_pass)
	    {
		Session::set('login', $login);
		Session::set('islogin', 1);
		Session::del('info');
		Session::del('error');
		Redirect::to('admin/admins');
	    }
	    else
	    {
		Session::set('error', 'Не верный логин или пароль');
		Redirect::to('admin/login');
	    }
	}
	else
	{
	    $params['titlepage'] = "Админский вход";
	    $params['H1'] = "Админский вход";
	    $params['action'] = "login";
	    View::disp($params, "admin.twig");
	}
    }

    public static function logout()
    {
	Session::destroy();
	Redirect::to('index');
    }

    public function admins()
    {
	Session::checkauth();
	$params = View::params("list", "Список админов", "Admins", "Просмотр спиcка админов");
	$params['admins'] = Admin::get('');
	View::disp($params, "admin.twig");
    }

    public function categories()
    {
	Session::checkauth();
	$params = View::params("list", "Список категорий", "Categories", "Просмотр спиcка категорий");
	$params['categories'] = Category::get();
	foreach ($params['categories'] as $key => $value)
	{
	    $id_category = $value['id_category'];
	    $value['count'] = [ 'posted' => Question::countQuestion('posted', $id_category),
		'hidden' => Question::countQuestion('hidden', $id_category),
		'moderate' => Question::countQuestion('moderate', $id_category),
	    ];

	    $params['categories'][$key] = $value;
	}
	View::disp($params, "admin.twig");
    }

    public function questions()
    {
	Session::checkauth();
	$params = View::params("list", "Список вопросов", "Questions", "Просмотр спиcка вопросов");
	$params['questions'] = Question::get();
	View::disp($params, "admin.twig");
    }

    public function unreadquestions()
    {
	Session::checkauth();
	$params = View::params("list", "Новые вопросы", "Questions", "Просмотр спиcка новых вопросов");
	$params['questions'] = Question::getUnreadQuestion();
	View::disp($params, "admin.twig");
    }

    public function authors()
    {
	Session::checkauth();
	$params = View::params("list", "Список авторов", "Authors", "Просмотр спиcка авторов");
	$params['authors'] = Authors::get();
	View::disp($params, "admin.twig");
    }

    public function edit()
    {
	Session::checkauth();
	$id_admin = Request::get('Admins');
	$id_category = Request::get('Categories');
	$id_question = Request::get('Questions');
	$id_author = Request::get('Authors');
	$category = Request::post('Category');
	$Admins_login = Request::post('Admins-login');
	$Admins_email = Request::post('Admins-email');
	$Admins_password = Request::post('Admins-pass');
	$Author_name = Request::post('Authors_name');
	$Author_email = Request::post('Authors_email');
	$Author_id = Request::post('id_users');
	$answer_id = Request::post('answer_id');
	$category_id = Request::post('id_category');
	$question_id = Request::post('question_id');
	$answer = Request::post('answer');
	$user = Request::post('user_name');
	$question = Request::post('question');
	if ($Admins_login !== FALSE && $Admins_password !== FALSE && $Admins_email !== FALSE)
	{
	    $id_admin = Request::post('id_admin');
	    if ($id_admin !== "1")
	    {
		$Admins_password = crypt($Admins_password, qwer);
		Admin::update($Admins_password, $Admins_login, $id_admin, $Admins_email);
		Redirect::to('admin/admins');
	    }
	}
	elseif ($Author_name !== FALSE && $Author_email !== FALSE && $Author_id !== FALSE)
	{
	    Authors::update($Author_id, $Author_email, $Author_name);
	    Redirect::to('admin/Authors');
	}
	elseif ($user !== FALSE && $question_id !== FALSE)
	{
	    $id_category = Request::post('id_category');

	    $status = Request::post('status');
	    if ($answer_id == '' && $answer !== '')
	    {
		Answer::set($answer, $question_id);
		Question::update($question_id, $question, $category_id, $status);
		Redirect::to('admin/Questions');
	    }
	    elseif (is_numeric($answer_id) && $answer !== '')
	    {
		Answer::update($answer, $question_id, $answer_id);
		Question::update($question_id, $question, $category_id, $status);
		Redirect::to('admin/Questions');
	    }
	    Question::update($question_id, $question, $category_id, $status);
	    Redirect::to('admin/Questions');
	}
	elseif ($category !== FALSE)
	{
	    $id_category = Request::post('id_category');
	    if ($id_category !== '1')
	    {
		Category::update($category, $id_category);
		Redirect::to('admin/Categories');
	    }
	}
	if (is_numeric($id_admin) && $id_admin !== '1')
	{
	    $params = View::params("edit", "Смена пароля админа", 'Admins', "Введите новый пароль админа");
	    $params['id_admin'] = $id_admin;
	    $admin = Admin::get($id_admin);
	    $params['admin_name'] = $admin[0]['login'];
	    $params['admin_email'] = $admin[0]['email'];
	    View::disp($params, "admin.twig");
	}
	elseif (is_numeric($id_category) && $id_category !== false)
	{
	    $params = View::params("edit", "Редактирование категории", 'Categories', "Введите название категории");
	    $category = Category::get($id_category);
	    $params['Category'] = $category[0]['category'];
	    $params['id_category'] = $id_category;
	    View::disp($params, "admin.twig");
	}
	elseif (is_numeric($id_question))
	{
	    $params = View::params("edit", "Редактирование вопроса", 'Questions', "Ответить или отредактировать вопрос");
	    $question = Question::get($id_question);
	    $answer = Answer::get($id_question);
	    $params['categories'] = Category::get();
	    $params['question'] = $question[0]['question'];
	    $params['id_question'] = $question[0]['id_question'];
	    $params['status'] = $question[0]['status'];
	    $params['category'] = $question[0]['category'];
	    $params['id_category'] = $question[0]['id_category'];
	    $params['name'] = $question[0]['name'];
	    $params['email'] = $question[0]['email'];
	    if (!empty($answer[0]['answer']))
	    {
		$params['answer'] = $answer[0]['answer'];
		$params['id_answer'] = $answer[0]['id_answer'];
	    }
	    View::disp($params, "admin.twig");
	}
	elseif (is_numeric($id_author))
	{
	    $params = View::params("edit", "Редактирование автора", 'Authors', "Редактирование автора");
	    $authors = Authors::get($id_author);
	    $params['id_users'] = $authors[0]['id_users'];
	    $params['name'] = $authors[0]['name'];
	    $params['email'] = $authors[0]['email'];
	    View::disp($params, "admin.twig");
	}
    }

    public function add()
    {
	Session::checkauth();
	$Admins_login = Request::post('Admins-login');
	$Admins_password = Request::post('Admins-pass');
	$Admins_mail = Request::post('Admins-mail');
	$category = Request::post('Category');
	$page = Request::get('new');
	if ($Admins_login !== FALSE && $Admins_password !== FALSE && $Admins_mail !== FALSE)
	{
	    $password = crypt($Admins_password, qwer);
	    Admin::set($Admins_login, $password, $Admins_mail);
	    Redirect::to('admin/admins');
	}
	elseif ($category !== False)
	{
	    Category::set($category);
	    Redirect::to('admin/Categories');
	}
	if ($page == "Admins")
	{
	    $params = View::params("add", "Добавить нового админа", $page, "Добавить нового админа");
	    View::disp($params, "admin.twig");
	}
	if ($page == "Categories")
	{
	    $params = View::params("add", "Добавить новую категорию", $page, "Добавить новую категорию");
	    View::disp($params, "admin.twig");
	}
    }

    public function del()
    {
	Session::checkauth();
	$id_admin = Request::get('Admins');
	$id_category = Request::get('Categories');
	$id_question = Request::get('Questions');
	$id_author = Request::get('Authors');
	if ($id_admin !== FALSE && is_numeric($id_admin) && $id_admin !== "1")
	{
	    Admin::delete($id_admin);
	    Redirect::to('admin/admins');
	}
	elseif ($id_category !== FALSE && is_numeric($id_category) && $id_category !== "1")
	{
	    Category::delete($id_category);
	    Redirect::to('admin/categories');
	}
	elseif ($id_question !== FALSE && is_numeric($id_question))
	{
	    Question::delete($id_question);
	    Redirect::to('admin/Questions');
	}
	elseif ($id_author !== FALSE && is_numeric($id_author))
	{
	    Authors::delete($id_author);
	    Redirect::to('admin/Authors');
	}
	else
	{
	    Redirect::to('404');
	}
    }

}
