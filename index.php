<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('display_errors', 1);
require_once __DIR__ . '/application/core/model.php';
require_once __DIR__ . '/application/core/view.php';
require_once __DIR__ . '/application/core/controller.php';
require_once __DIR__ . '/application/core/route.php';
require_once __DIR__ . '/application/core/Request.php';
require_once __DIR__ . '/application/core/Session.php';
require_once __DIR__ . '/application/core/Redirect.php';
require_once __DIR__ . '/application/core/DatabaseFactory.php';
require_once __DIR__ . '/vendor/autoload.php';
Session::init();

Route::start();

